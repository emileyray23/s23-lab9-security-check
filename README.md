# Lab9 -- Security check

## My site

<https://my.university.innopolis.ru/>

### Test Forget Password

<https://cheatsheetseries.owasp.org/cheatsheets/Forgot_Password_Cheat_Sheet.html#forgot-password-cheat-sheet>

| Test step  | Result |
|---|---|
| Open the site | Ok |
| Sing out since I was logged in  | Logged out. An informational page opened, I had to open the target page again |
| Tap "Change Password" | OK, new page opened |
| Type fake email, password, new password | The account did not exist, the site complained about it |
| Type my email, password, new password |  Ok, the site updated my passwor and let me know |

The time of response was the same for both cases. And for different cases, the site showed different messages. There was no restriction on the
amount of tries, so emails might be found out by massive requests.

### Test Response Headers

<https://cheatsheetseries.owasp.org/cheatsheets/HTTP_Headers_Cheat_Sheet.html>

| Test step  | Result |
|---|---|
| Open the site | Ok |
| Press F12 to see DevTools | Ok |
| Login | Ok |
| Check X-Frame-Options prseense | No header found |
| Check X-XSS-Protection presense | No header found |
| Check X-Content-Type-Options presense | No header found |
| Check Referrer-Policy | `strict-origin-when-cross-origin` |
| Check Content-Type presense | `text/html; charset=utf-8,text/html; charset=utf-8` |
| Check Set-Cookie presense | OK, It is present (too long) |

The site has no custom (X) headers that are recommended. However it has all the required headers in a form that's recommended to be.
